-- Double Dactorial Function.
facDouble :: Int -> Int
facDouble 0 = 1
facDouble 1 = 1
facDouble n = product [n, (n-2) .. 1]

-- List of x n times
repeatIt :: Int -> a -> [a]
repeatIt n x = [x|_<-[1..n]]

-- Powering x by y
naturalPower :: (Num a) => a->a->a
naturalPower x y = x * (naturalPower x (y-1))

-- Fourth
linking :: [[a]] -> [a]
linking [] = []
linking xxs = (xxs !! 0)++(linking (tail xxs)) 

-- Fifth
getElement :: [a] -> Int -> a
getElement xs x = xs !! x