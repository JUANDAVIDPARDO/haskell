import Data.Char

-- Actividad 2 

-- punto 1
-- Este algoritmo cambia n elementos de un conjunto y los intercambia con otro ej: 2 elemento de "Juan" "Pardo" -> "Paan" "Jurdo".
functionOne:: Int -> [a] -> [a] -> ([a],[a]) 
functionOne x a b = ((take x b )++(reverse(take (length(a)-x) (reverse a))) , (take x a )++(reverse(take (length(b)-x) (reverse b)))) 


-- punto 2
-- Este algoritmo busca números en una cadena de String y retorna su sumatoria.
functionTwo::String -> Int
functionTwo [] = 0
functionTwo x = if isDigit(head x)
                then digitToInt(head x) + functionTwo(tail x)
                else functionTwo(tail x)