doubleMe x = x + x
doubleUs x y = doubleMe x + doubleMe y
doubleSmallNumber x =   if x > 100 
                        then x 
                        else x * 2
b = [1,2,3,4,5]


-- Double Dactorial Function.
facDouble :: Int -> Int
facDouble 0 = 1
facDouble 1 = 1
facDouble n = product [n, (n-2) .. 1]

-- List of x n times
repeatIt :: Int -> a -> [a]
repeatIt n a = [a|_<-[1..n]]