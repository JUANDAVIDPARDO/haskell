import Data.Char

-- Actividad 2 

-- punto 2
-- Este algoritmo busca números en una cadena de String y retorna su sumatoria.
functionTwo::String -> Int
functionTwo [] = 0
functionTwo x =  if isDigit(head x)
                      then digitToInt(head x) + functionTwo(tail x)
                      else functionTwo(tail x)